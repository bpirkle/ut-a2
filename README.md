ut-a2-vagrant
==================

Ubuntu (Trusty) + Aegir 2 Vagrant box

1. Download and Install Virtualbox (https://www.virtualbox.org/wiki/Downloads)
2. Download and Install Vagrant (https://www.vagrantup.com/downloads.html)
3. Clone this repo into a directory of your choice (ex. C:\Users\<your user name>\VMs\devbox1)
4. Gotcha warning:  if you've cloned this repo into Windows, your line ends for all the scripts
   in the "files" directory may have just been converted (by git) from Linux-style (\n) to 
   Windows-style (\r\n).  Which means they won't run when we need them later.  You can either
   convert them now in Windows (using the editor of your choice that offers this option),
   or convert them in Linux after the box is set up.  The scripts end up in /usr/local/bin.
   A quick one-liner to convert them in Linux is:  sudo vi +':w ++ff=unix' +':q' <file-name>
5. Create an empty directory named "vagrant-xfer" at that same directory level
6. Create Virtual Machine.  From within the ut-a2-vagrant repo:  "vagrant up"
7. Profit

```Shell
$ vagrant up
$ vagrant ssh # To access your vagrant box.
```

##File transfers
The "vagrant_xfer" directory that you created simply serves as a convenient way to
get files into and out of the vagrant box.  There are umpteen other ways to do this,
so feel free to use a different method if you prefer.