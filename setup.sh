#!/usr/bin/env bash

# You may want to execute this from a command line, just in case you ever
# need real root access:
#   sudo passwd root  
# Should I script that in?  It is just a dev box, after all...

FILE=~/install_done

if [ ! -f $FILE ]; then
  echo ">>>>>> Copying custom utility scripts..."

  scp /vagrant/files/lamp-setup /usr/local/bin/
  chmod +x /usr/local/bin/lamp-setup

  scp /vagrant/files/make-aegir-user /usr/local/bin/
  chmod +x /usr/local/bin/make-aegir-user

  scp /vagrant/files/aegir-setup /usr/local/bin/
  chmod +x /usr/local/bin/aegir-setup

  scp /vagrant/files/java-setup /usr/local/bin/
  chmod +x /usr/local/bin/java-setup

  scp /vagrant/files/intellij-setup /usr/local/bin/
  chmod +x /usr/local/bin/intellij-setup
  scp /vagrant/files/intellij-settings.jar /usr/local/bin/
  
  scp /vagrant/files/xdebug-setup /usr/local/bin/
  chmod +x /usr/local/bin/xdebug-setup
  
  scp /vagrant/files/redis-setup /usr/local/bin/
  chmod +x /usr/local/bin/redis-setup
    
  scp /vagrant/files/nodejs-setup /usr/local/bin/
  chmod +x /usr/local/bin/nodejs-setup
  
  scp /vagrant/files/mysql-lock-fix /usr/local/bin/
  chmod +x /usr/local/bin/mysql-lock-fix

  # Always a good idea...
  sudo apt-get update

  # Install DRM (needed for vboxvideo driver to work)
  #   https://forums.virtualbox.org/viewtopic.php?f=6&t=64386
  sudo apt-get install linux-image-extra-`uname -r`

  # Install a GUI.  "ubuntu-desktop" gives full Unity, tweak as you see fit.
  # Some alternatives (untested):
  #   Slimmer Unity:  sudo apt-get -y install  --no-install-recommends ubuntu-desktop  
  #   KDE:  sudo apt-get -y install kubuntu-desktop
  #   XFCE:  sudo apt-get -y install xubuntu-desktop
  #   Slimmer XFCE:  sudo apt-get -y install xfce4  
  #   GNOME:  sudo apt-get -y install gnome-shell gdm
  #   LXDE (lightweight GUI):  sudo apt-get -y install lxde
  #sudo aptitude -y install ubuntu-desktop
  echo ">>>>>> Installing GUI..."  
#  sudo apt-get -y install ubuntu-desktop
  sudo apt-get -y install lxde
# Note:  Unity didn't give the aegir user a GUI login option.  Is that because
# we set a nonstandard home directory?  Need to Google for how to enable "more" 
# on the Unity login screen.

  # Some other stuff you may want, if your GUI didn't include it (untested):
#  sudo apt-get -y install firefox  
  sudo apt-get -y install chromium-browser
#  sudo apt-get -y install flashplugin-installer
#  sudo apt-get -y install pepperflashplugin-nonfree
#  sudo update-pepperflashplugin-nonfree --install  
#  sudo apt-get -y install chromium-codecs-ffmpeg-extra  
#  sudo apt-get -y install libreoffice
#  sudo apt-get -y install ubuntu-restricted-extras  
#  sudo apt-get -y install filezilla
#  sudo apt-get -y install lxtask

  touch $FILE;
  echo ">>>> Vagrant setup complete.  You have a basic Ubuntu box, but no dev stack."  
  echo ">>>> Do 'vagrant reload' to relaunch with a GUI"
  echo ">>>> The password for vagrant user is 'vagrant'"  
  echo ">>>> For a LAMP stack, run 'lamp-setup' from within your vagrant box."
  echo ">>>> Then for an Aegir install, run 'make-aegir-user' then 'aegir-setup'."  
fi

